library file1;
import 'package:flutter/material.dart';
part 'file2.dart';

// file ini digunakan untuk mengatur pengaturan warna, textStyle, dan juga fontWeight

double defaultMargin = 24.0;
///Pengaturan Warna
Color grey1Color = const Color(0xff757575);
Color grey3Color = const Color(0xff9E9E9E);
Color grey5Color = const Color(0xffF7FAFC);
Color white1Color = const Color(0xffFFFFFF);
Color white2Color = const Color(0xffF3F3F3);
Color green1Color = const Color(0xff069550);
Color blackColor = const Color(0xff202020);
Color lightblud = const Color(0xffE7F5FF);
Color yellow1 = const Color(0xffFFC939);

Color transparent = Colors.transparent;

///Pengaturan TextStyle
TextStyle grey1TextStyle = TextStyle(color: grey1Color);
TextStyle grey3TextStyle = TextStyle(color: grey3Color);
TextStyle blackTextStyle = TextStyle(color: blackColor);
TextStyle white1TextStyle = TextStyle(color: white1Color);
TextStyle green1TextStyle = TextStyle(color: green1Color);

///Pengaturan FontWeight
FontWeight light = FontWeight.w300;
FontWeight regular = FontWeight.w400;
FontWeight medium = FontWeight.w500;
FontWeight semiBold = FontWeight.w600;
FontWeight bold = FontWeight.w700;
FontWeight extraBold = FontWeight.w800;
FontWeight thick = FontWeight.w900;
