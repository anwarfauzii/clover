import 'package:feda_clover/shared/theme.dart';
import 'package:feda_clover/widgets/custom_bottom_navbar.dart';
import 'package:flutter/material.dart';

class BottomNavbar extends StatelessWidget {
  /// Class BottomNavbar menampilkan menu Home, Chat, dan Profile, di bagian bawah halaman
  const BottomNavbar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        width: double.infinity,
        height: 61,
        color: grey5Color,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: const [
            CustomBottomNavbar(
              imageUrl: 'assets/icon_home.png',
              isSelected: true,
            ),
            CustomBottomNavbar(imageUrl: 'assets/icon_chat.png'),
            CustomBottomNavbar(imageUrl: 'assets/icon_profile.png'),
          ],
        ),
      ),
    );
  }
}
