import 'package:feda_clover/shared/theme.dart';
import 'package:flutter/material.dart';

/// Class CustomBottomNavbar untuk menampilkan icon Menu di bagian bawah
class CustomBottomNavbar extends StatelessWidget {
  /// field ini digunakan untuk menampilkan icon dan apakah icon itu dipilih atau tidak
  final String imageUrl;
  final bool isSelected;

  /// * [imageUrl] bertipe **String** berisi *Url* icon yang akan ditampilkan
  /// 
  ///  * [isSelected] bertipe **boolean**, berisi apakah icon tsb dipilih atau tidak, defaultnya *false*
  /// 
  /// Contoh :
  /// 
  /// ```
  /// CustomBottomNavbar(
  /// imageUrl: 'assets/icon_home.png', 
  /// isSelected: true,
  /// ),
  /// ```
  const CustomBottomNavbar({Key? key,required this.imageUrl, this.isSelected = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        const SizedBox(),
        Container(
          width: 24,
          height: 24,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(imageUrl), //Penempatan imageUrl yang akan diminta untuk di isi
            ),
          ),
        ),
        Container(
          width: 27,
          height: 3,
          decoration: BoxDecoration(
            color: isSelected ? green1Color : transparent, //Pemilihan apakah icon tsb di pilih atau tidak, jika tidak dipilih defaultnya adalah warna transparent
            borderRadius: BorderRadius.circular(12),
          ),
        )
      ],
    );
  }
}
