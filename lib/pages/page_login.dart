import 'package:feda_clover/shared/bottom_navbar.dart';
import 'package:feda_clover/shared/theme.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatelessWidget {
  /// Class LoginPage akan menampilkan layar Login untuk User
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Widget inputFullname digunakan untuk menampilkan textfield untuk mengisi nama lengkap
    Widget inputUsername() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Username',
            style: blackTextStyle.copyWith(fontSize: 16),
          ),
          const SizedBox(height: 8),
          TextFormField(
            cursorColor: blackColor,
            decoration: InputDecoration(
              hintText: 'Username',
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(color: green1Color),
              ),
            ),
          )
        ],
      );
    }
    // Widget inputPassword digunakan untuk menampilkan textfield untuk mengisi password
    Widget inputPassword() {
      return Container(
        margin: const EdgeInsets.only(top: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Password',
                  style: blackTextStyle.copyWith(
                      fontSize: 15, fontWeight: regular),
                ),
                TextButton(
                  onPressed: () {},
                  child: Text(
                    'Lupa password?',
                    style: green1TextStyle.copyWith(fontSize: 16),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 8),
            TextFormField(
              cursorColor: blackColor,
              obscureText: true,
              decoration: InputDecoration(
                hintText: 'Password',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: green1Color),
                ),
              ),
            )
          ],
        ),
      );
    }
    // Widget title digunakan untuk menampilkan judul untuk login
    Widget title() {
      return Container(
        margin: const EdgeInsets.only(top: 66),
        child: Column(
          children: [
            Text(
              'Masuk',
              style: blackTextStyle.copyWith(fontSize: 32, fontWeight: bold),
            ),
            const SizedBox(height: 16),
            Text.rich(
              TextSpan(
                text: 'Silahkan masuk ke akun',
                style: blackTextStyle.copyWith(fontSize: 16),
                children: [
                  TextSpan(
                    text: ' Klover ',
                    style: green1TextStyle.copyWith(
                        fontSize: 16, fontWeight: bold),
                  ),
                  TextSpan(
                    text: 'Anda yang sudah terdaftar!',
                    style: blackTextStyle.copyWith(fontSize: 16),
                  ),
                ],
              ),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      );
    }
    // Widget textFormField digunakan untuk menampilkan  FormField untuk mengisi username dan password
    Widget textFormField() {
      return Container(
        margin: const EdgeInsets.only(top: 32),
        child: Column(
          children: [
            inputUsername(),
            inputPassword(),
          ],
        ),
      );
    }
    // Widget buttonProcess digunakan untuk menampilkan tombol untuk Login
    Widget buttonProcess() {
      return Container(
        margin: const EdgeInsets.only(top: 32, bottom: 151),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            TextButton(
              onPressed: () => Navigator.pushNamed(context, '/register'),
              child: Text.rich(TextSpan(
                  text: 'Belum punya akun? ',
                  style: blackTextStyle.copyWith(fontSize: 16),
                  children: [
                    TextSpan(
                        text: 'Daftar',
                        style: green1TextStyle.copyWith(
                            fontSize: 16, fontWeight: bold))
                  ])),
            ),
            Container(
              width: 80,
              height: 42,
              margin: const EdgeInsets.only(right: 16),
              child: TextButton(
                onPressed: () {
                  Navigator.pushNamed(context, '/main');
                },
                style: TextButton.styleFrom(
                  backgroundColor: green1Color,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                ),
                child: Text(
                  'Masuk',
                  style: white1TextStyle.copyWith(fontSize: 16),
                ),
              ),
            ),
          ],
        ),
      );
    }

  
    return Scaffold(
        body: SafeArea(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: defaultMargin),
        child: ListView(
          children: [
            title(),
            textFormField(),
            buttonProcess(),
            const BottomNavbar(),
          ],
        ),
      ),
    ));
  }
}
